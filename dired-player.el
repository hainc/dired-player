;;; dired-player.el --- Play songs in a dired buffer  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Hai NGUYEN

;; Author: Hai NGUYEN <haiuyeng@gmail.com>
;; Keywords: files, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'dired)


(defcustom dired-player-dir nil "Directory of songs.")
(defvar dired-player-mode-map (make-sparse-keymap))

(dolist (info '(("<return>" dired-player-play)))
  (define-key dired-player-mode-map (kbd (car info)) (cadr info)))

(define-minor-mode dired-player-mode "Play songs in a dired buffer."
  nil ; init value
  nil ; lighter
  dired-player-mode-map ; keymap
  )

(defun dired-player (&optional song-dir)
  (interactive
   (list (and (null dired-player-dir)
              (setq dired-player-dir (read-directory-name "Song directory: ")))))
  (dired dired-player-dir)
  (dired-player-mode 1))

(defun dired-player-play (&optional song-fname-s loopp play-command)
  (interactive
   (let* ((fnames (dired-get-marked-files)))
     (list fnames current-prefix-arg (dired-guess-default fnames))))
  (catch 'break
    (dired-do-async-shell-command play-command nil song-fname-s)
     (unless loopp
        (throw 'break nil))))

(provide 'dired-player)
;;; dired-player.el ends here
